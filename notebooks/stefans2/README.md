# Stefan Worklog

- [2024-2-6 - Meeting with Selva](#2024-2-6---meeting-with-selva)
- [2024-2-7 - Team Meeting](#2024-2-7---team-meeting)
- [2024-2-12 - 2nd Meeting with Selva](#2024-2-12---2nd-meeting-with-selva)
- [2024-2-22 - PCB Workday 1](#2024-2-22---pcb-workday-1)
- [2024-2-28 - PCB Workday 2](#2024-2-28---pcb-workday-2)
- [2024-3-4 - Design Review](#2024-3-4---design-review)
- [2024-3-21 - PCB Arrival and Assembly](#2024-3-21---pcb-arrival-and-assembly)
- [2024-3-25 - Hardware/Software Verification](#2024-3-25---hardwaresoftware-verification)
- [2024-4-2 - Half Bridge Testing](#2024-4-2---half-bridge-testing)
- [2024-4-6 - Rev 1.2 PCB Design](#2024-4-6---rev-12-pcb-design)
- [2024-4-16 - Mock Demo](#2024-4-16---mock-demo)
- [2024-4-19 - New PCB Revision Assembly](#2024-4-19---new-pcb-revision-assembly)
- [2024-4-22 - Long Work Day and Final Assembly](#2024-4-22---long-work-day-and-final-assembly)


## 2024-2-6 - Meeting with Selva

### Objectives
Review design ideas with Selva and speak about what is possible with the ECE 445 resources.

### What was done
We went over the design which we were planning on doing and decided to work within the confines of what the service shop has to offer. The STM32F401RE is our microcontroller and we decided on the following high level requirements:

1. GUI for users to interact
2. Cooling system with temperature minimum
3. Heating system with temperature maximum
4. Tolerance for holding temperature

These requirements might be changed at a later date. Now it is time to begin selecting parts for our high level requirements.

## 2024-2-7 - Team Meeting

### Objectives
Determine what parts we have and what parts we have to find to achieve our design.

### What was done
Found a 24V PSU in existing parts box. Confirmed power output of the 24V supply was as expected, we can easily achieve 100W. Went down to the machine shop to confirm mechanical design and whether it was possible or not. Initial design looks possible to do and there should be no major machining problems. Electrical still needs to find all of the power electronics and logic level parts for the design.

![Confirmatino of 24V Supply](images/24VConfirmation.jpg)

## 2024-2-12 - 2nd Meeting with Selva

### Objectives 
Check in with progress and talk about logistics.

### What was done

Met up with machine shop afterwards to confirm modified mechanical design. Figured out board and parts logistics with Selva, confirmed what parts we can get and which ones we can buy and how much of our budget was used.

## 2024-2-22 - PCB Workday 1

### Objectives
Get started on PCB work and schematics.

### What was done
Existing component were imported into the design, all of the custom footprints were found and the basic heirarchical sheet layout has been set up. List of service shop components has also been created and is ready to be sent to Selva.

## 2024-2-28 - PCB Workday 2

### Objectives
Finish up PCB schematics and begin layout.

### What was done
PCB Schematics are done and layout has begun, no major issues during the layout, some unclear datasheets but all in all, fairly standard PCB schematic capture.

![High Level Sheet](images/HighLevelSchematic.png)

## 2024-3-4 - Design Review

### Objectives
Finish board layout and get constructive criticism on design

### What was done
Board layout was successfully finished and presented in front of Selva and the team. They had no major issues with the design and gave me the green light to proceed with ordering the boards. I will put the board orders in before the end of the day.

![PCB Design Rev 1](images/Rev1PCB.png)

## 2024-3-21 - PCB Arrival and Assembly

### Objectives
Assemble the board and test the systems as I assemble.

### What was done
Buck converters were assembled, initial ones exploded when powered. I determined that this was an issue with the Enable pin being pulled high to 24V. Needed to add a bodge wire in order to make the Design work. Once bodge wire assembled, the buck converters work without major issue. I don't have enough time to assemble the rest of the board today but will do it later.

![Buck Bodge](images/BuckBodge.png)

## 2024-3-25 - Hardware/Software Verification

### Objectives
Finish logic level board assembly and verify the software and hardware system interaction.

### What was done
I finished assembling the whole board and moved on to flashing the STM. There don't seem to be any layout issues, as the STM flashed with no issue and shows up in my CubeIDE. Tested on board LED, Relays, and finally verified the power systems that they are up to our specifications.

![Power Verification](images/PowerVerification.png)

## 2024-4-2 - Half Bridge Testing

### Objectives
Determine if the cooling system half bridges function properly.

### What was done
I connected the outputs to the oscilloscope and I was able to get 50% PWM on the output as expected. The MOSFET pinouts were also incorrect, so I had to bodge wire the mosfets so that they have the correct pinout. But when adding output capacitance, the gate drivers exploded and caused an internal short, replaced them and I was getting the same issue. I can not stay longer in the lab but I will do more research on the topic and attempt to find a solution.

![PWM Output Oscilloscope Capture](images/PWMOutput.png)
![Dead Gate Driver](images/DeadGateDriver.png)

## 2024-4-6 - Rev 1.2 PCB Design

### Objectives
Design PCB with quality of life changes and submit it to order before the 5th board deadline

### What was done
The final PCB was designed and submitted in time for the board deadline. It had a few changes, most notably, the option for a P channel, N Channel driving circuit for the MOSFETs, which should be able to fix the output capacitance problem we have. I also fixed the incorrect MOSFET pinouts as well as added the proper resistor divider for the Buck Converters.

![New PN Layout](images/newPNSchematic.png)

## 2024-4-16 - Mock Demo

### Objectives
Give the TA a mock demo and show the capabilites of the device in a controlled environment and fully integrated.

### What was done
We showed selva our cooling and heating capabilities and were told it has a lot of potential but we have to fully integrate everything before the final demo. We didn't have full cooling power working but the rest of the systems functioned fine. We were able to reach cold temperatures, but not freezing just yet, will need to keep working on it for the final demo in just a week!

![Wires Hanging Out](images/VerificationSetup.png)
![Reaching 10 degrees](images/Reaching10Degrees.jpg)

## 2024-4-19 - New PCB Revision Assembly

### Objectives
Assemble the new revision boards that came in and test various functionality.

### What was done
After a long day in the lab and a lot of testing of the new PN Channel half bridge, I determined the heating curve to be too long. We have decided to move to a LC network on the output of the bootstrapped half bridge converter to allow us to have the proper voltage on the output and still smooth it. 

![PN Channel Layout](images/NewPNLayout.png)
![PN Channel MOSFETS](images/PNChannelMOSFET.png)
![PN Channel Testing](images/PNChannelTesting.png)
![LC Network](images/LCNetwork.png)

## 2024-4-22 - Long Work Day and Final Assembly

### Objectives
Assemble the final board in a working state and fully integrate it.

### What was done
This final day was very hectic. I assembled the new board with the existing half bridge setup and then ran it under load. The MOSFETs overheat quite a bit, so we switched to a heatsink on the board. We then measured temperatures at various PWM value and determined we needed to keep the PWM below a certain value in order to keep a safe temperature on the MOSFETs. 

![Heatsink Picture](images/Heatsink.png)
![Heating Graph](images/HeatsinkGraph.png)

After that, we finally tested the MOSFETs at a lower PWM and installed and wired all of the electronics into the custom Ebox. The project looks great and some of the electrical systems may not work exactly to full specification, but we have a fully working system. Our next stop is the demo tomorrow!

![Board finally Wired](images/BoardinBox.png)